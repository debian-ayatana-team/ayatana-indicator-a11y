Source: ayatana-indicator-a11y
Section: misc
Priority: optional
Maintainer: Ayatana Packagers <pkg-ayatana-devel@lists.alioth.debian.org>
Uploaders:
 Mike Gabriel <sunweaver@debian.org>,
 Debian UBports Team <team+ubports@tracker.debian.org>,
Build-Depends: cmake,
               cmake-extras (>= 0.10),
               libaccountsservice-dev,
               libayatana-common-dev,
               libglib2.0-dev (>= 2.36),
               libx11-dev,
               libxrandr-dev,
# for packaging
               debhelper-compat (= 13),
               dpkg-dev (>= 1.16.1.1),
               intltool,
# for systemd unit
               systemd-dev [linux-any],
Standards-Version: 4.7.2
Rules-Requires-Root: no
Homepage: https://github.com/AyatanaIndicators/ayatana-indicator-a11y
Vcs-Git: https://salsa.debian.org/debian-ayatana-team/ayatana-indicator-a11y.git
Vcs-Browser: https://salsa.debian.org/debian-edu-ayatana-team/ayatana-indicator-a11y

Package: ayatana-indicator-a11y
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         onboard,
         orca,
         espeak-ng,
Description: Ayatana Indicator for Accessibility Settings
 This Ayatana Indicator provides quick access to accessibility related
 assistance applications such as the screen reader or an on-screen
 keyboard.
 .
 The provided accessibility indicator should show as an icon in the top
 panel of indicator aware desktop environments. It can be used to toggle
 and access various accessibility features.
 .
 Ayatana Indicators are only available on desktop environments that
 provide a renderer for system indicators (such as MATE, Xfce, Lomiri,
 etc.).
